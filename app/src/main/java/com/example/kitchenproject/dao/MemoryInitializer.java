package com.example.kitchenproject.dao;

import com.example.kitchenproject.domain.Eye;
import com.example.kitchenproject.domain.Oven;

public class MemoryInitializer {

    protected void eraseData() {
        for(Eye eye : getEyeDAO().findAll()) {
            getEyeDAO().delete(eye);
        }
        getOvenDAO().delete();
    }

    public void prepareData() {
        eraseData();

        Eye eye1 = new Eye(1);
        Eye eye2 = new Eye(2);
        Eye eye3 = new Eye(3);
        Eye eye4 = new Eye(4);

        EyeDAO eyeDAO = getEyeDAO();
        eyeDAO.save(eye1);
        eyeDAO.save(eye2);
        eyeDAO.save(eye3);
        eyeDAO.save(eye4);

        Oven oven = new Oven();

        OvenDAO ovenDAO = getOvenDAO();
        ovenDAO.save(oven);
    }

    public EyeDAO getEyeDAO() {
        return new EyeDAO();
    };

    public OvenDAO getOvenDAO() {
        return new OvenDAO();
    };
}