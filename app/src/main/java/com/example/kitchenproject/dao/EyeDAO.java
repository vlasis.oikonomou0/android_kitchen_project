package com.example.kitchenproject.dao;

import com.example.kitchenproject.domain.Eye;

import java.util.ArrayList;
import java.util.List;

public class EyeDAO {
    protected static ArrayList<Eye> eyes = new ArrayList<Eye>();

    public void delete(Eye entity) {
        eyes.remove(entity);
    }

    public List<Eye> findAll() {
        ArrayList<Eye> result = new ArrayList<Eye>();
        result.addAll(eyes);
        return result;
    }

    public void save(Eye entity) {
        eyes.add(entity);
    }

    public Eye find(int eyeNumber) {
        for(Eye eye : eyes)
            if(eye.getEyeNumber() == eyeNumber)
                return eye;

        return null;
    }
}
