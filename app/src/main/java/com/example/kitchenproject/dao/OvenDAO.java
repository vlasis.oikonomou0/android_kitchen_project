package com.example.kitchenproject.dao;

import com.example.kitchenproject.domain.Eye;
import com.example.kitchenproject.domain.Oven;

import java.util.ArrayList;
import java.util.List;

public class OvenDAO {
    protected static Oven oven;

    public void delete() {
        oven = null;
    }

    public void save(Oven ov) {
        oven = ov;
    }

    public Oven get() {
        return oven;
    }
}
