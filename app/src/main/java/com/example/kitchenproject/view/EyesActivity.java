package com.example.kitchenproject.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.kitchenproject.R;
import com.example.kitchenproject.dao.EyeDAO;
import com.example.kitchenproject.domain.Setting;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class EyesActivity extends AppCompatActivity {
    EyeDAO eyeDAO = new EyeDAO();

    private Button eye1;
    private Button eye2;
    private Button eye3;
    private Button eye4;

    private Boolean eye1Flag;
    private Boolean eye2Flag;
    private Boolean eye3Flag;
    private Boolean eye4Flag;

    CountDownTimer countDownTimer1;
    CountDownTimer countDownTimer2;
    CountDownTimer countDownTimer3;
    CountDownTimer countDownTimer4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setContentView(R.layout.activity_eyes);

        setTitle(R.string.eyes);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        eye1 = findViewById(R.id.eyes);
        eye2 = findViewById(R.id.eyes2);
        eye3 = findViewById(R.id.eyes5);
        eye4 = findViewById(R.id.eyes6);

        eye1Flag = false;
        eye2Flag = false;
        eye3Flag = false;
        eye4Flag = false;

        eye1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(EyesActivity.this, EyeSettingsActivity.class);
                intent.putExtra("eyeId", 1);
                startActivity(intent);
            }
        });

        eye2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(EyesActivity.this, EyeSettingsActivity.class);
                intent.putExtra("eyeId", 2);
                startActivity(intent);
            }
        });

        eye3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(EyesActivity.this, EyeSettingsActivity.class);
                intent.putExtra("eyeId", 3);
                startActivity(intent);
            }
        });

        eye4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(EyesActivity.this, EyeSettingsActivity.class);
                intent.putExtra("eyeId", 4);
                startActivity(intent);
            }
        });

        if (eyeDAO.find(1).getSetting() == Setting.ON) {
            eye1.setText(Html.fromHtml("<b>ANOIKTO<br><small>♨ "+eyeDAO.find(1).getVolume()+"</small></b>"));
            eye1.setTextColor(getResources().getColor(R.color.green));

            if (eyeDAO.find(1).getAutoOff() != null) {
                countDownTimer1 = new CountDownTimer(getDateDiff(Calendar.getInstance().getTime(), eyeDAO.find(1).getAutoOff(), TimeUnit.MILLISECONDS), 1000) {
                    public void onTick(long millisUntilFinished) {

                        if (eye1Flag) {
                            cancel();
                            return;
                        }

                        NumberFormat f = new DecimalFormat("00");
                        long hour = (millisUntilFinished / 3600000) % 24;
                        long min = (millisUntilFinished / 60000) % 60;
                        long sec = (millisUntilFinished / 1000) % 60;
                        String hourS = "";
                        if (hour != 0) {
                            hourS = f.format(hour) + ":";
                        }
                        String minS = "";
                        if (min != 0 || hour == 0) {
                            minS = f.format(min) + ":";
                        }
                        eye1.setText(Html.fromHtml("<b>ANOIKTO<br><small>♨ "+eyeDAO.find(1).getVolume()+" • "+ hourS + minS + f.format(sec) +"</small></b>"));

                    }

                    public void onFinish() {
                        eye1.setText("ΚΛΕΙΣΤΟ");
                        eye1.setTextColor(getResources().getColor(R.color.buttons_color));
                    }
                }.start();
            }
            else {
                eye1Flag = true;
                eye1.setText(Html.fromHtml("<b>ANOIKTO<br><small>♨ "+eyeDAO.find(1).getVolume()+"</small></b>"));
            }
        }
        else {
            eye1Flag = true;
            eye1.setText("ΚΛΕΙΣΤΟ");
            eye1.setTextColor(getResources().getColor(R.color.buttons_color));
        }

        if (eyeDAO.find(2).getSetting() == Setting.ON) {
            eye2.setText(Html.fromHtml("<b>ANOIKTO<br><small>♨ "+eyeDAO.find(2).getVolume()+"</small></b>"));
            eye2.setTextColor(getResources().getColor(R.color.green));

            if (eyeDAO.find(2).getAutoOff() != null) {
                countDownTimer2 = new CountDownTimer(getDateDiff(Calendar.getInstance().getTime(), eyeDAO.find(2).getAutoOff(), TimeUnit.MILLISECONDS), 1000) {
                    public void onTick(long millisUntilFinished) {

                        if (eye2Flag) {
                            cancel();
                            return;
                        }

                        NumberFormat f = new DecimalFormat("00");
                        long hour = (millisUntilFinished / 3600000) % 24;
                        long min = (millisUntilFinished / 60000) % 60;
                        long sec = (millisUntilFinished / 1000) % 60;
                        String hourS = "";
                        if (hour != 0) {
                            hourS = f.format(hour) + ":";
                        }
                        String minS = "";
                        if (min != 0 || hour == 0) {
                            minS = f.format(min) + ":";
                        }
                        eye2.setText(Html.fromHtml("<b>ANOIKTO<br><small>♨ "+eyeDAO.find(2).getVolume()+" • "+ hourS + minS + f.format(sec) +"</small></b>"));
                    }

                    public void onFinish() {
                        eye2.setText("ΚΛΕΙΣΤΟ");
                        eye2.setTextColor(getResources().getColor(R.color.buttons_color));
                    }
                }.start();
            }
            else {
                eye2Flag = true;
                eye2.setText(Html.fromHtml("<b>ANOIKTO<br><small>♨ "+eyeDAO.find(2).getVolume()+"</small></b>"));
            }
        }
        else {
            eye2Flag = true;
            eye2.setText("ΚΛΕΙΣΤΟ");
            eye2.setTextColor(getResources().getColor(R.color.buttons_color));
        }

        if (eyeDAO.find(3).getSetting() == Setting.ON) {
            eye3.setText(Html.fromHtml("<b>ANOIKTO<br><small>♨ "+eyeDAO.find(3).getVolume()+"</small></b>"));
            eye3.setTextColor(getResources().getColor(R.color.green));

            if (eyeDAO.find(3).getAutoOff() != null) {
                countDownTimer3 = new CountDownTimer(getDateDiff(Calendar.getInstance().getTime(), eyeDAO.find(3).getAutoOff(), TimeUnit.MILLISECONDS), 1000) {
                    public void onTick(long millisUntilFinished) {

                        if (eye3Flag) {
                            cancel();
                            return;
                        }

                        NumberFormat f = new DecimalFormat("00");
                        long hour = (millisUntilFinished / 3600000) % 24;
                        long min = (millisUntilFinished / 60000) % 60;
                        long sec = (millisUntilFinished / 1000) % 60;
                        String hourS = "";
                        if (hour != 0) {
                            hourS = f.format(hour) + ":";
                        }
                        String minS = "";
                        if (min != 0 || hour == 0) {
                            minS = f.format(min) + ":";
                        }
                        eye3.setText(Html.fromHtml("<b>ANOIKTO<br><small>♨ "+eyeDAO.find(3).getVolume()+" • "+ hourS + minS + f.format(sec) +"</small></b>"));
                    }

                    public void onFinish() {
                        eye3.setText("ΚΛΕΙΣΤΟ");
                        eye3.setTextColor(getResources().getColor(R.color.buttons_color));
                    }
                }.start();
            }
            else {
                eye3Flag = true;
                eye3.setText(Html.fromHtml("<b>ANOIKTO<br><small>♨ "+eyeDAO.find(3).getVolume()+"</small></b>"));
            }
        }
        else {
            eye3Flag = true;
            eye3.setText("ΚΛΕΙΣΤΟ");
            eye3.setTextColor(getResources().getColor(R.color.buttons_color));
        }

        if (eyeDAO.find(4).getSetting() == Setting.ON) {
            eye4.setText(Html.fromHtml("<b>ANOIKTO<br><small>♨ "+eyeDAO.find(4).getVolume()+"</small></b>"));
            eye4.setTextColor(getResources().getColor(R.color.green));

            if (eyeDAO.find(4).getAutoOff() != null) {
                countDownTimer4 = new CountDownTimer(getDateDiff(Calendar.getInstance().getTime(), eyeDAO.find(4).getAutoOff(), TimeUnit.MILLISECONDS), 1000) {
                    public void onTick(long millisUntilFinished) {

                        if (eye4Flag) {
                            cancel();
                            return;
                        }

                        NumberFormat f = new DecimalFormat("00");
                        long hour = (millisUntilFinished / 3600000) % 24;
                        long min = (millisUntilFinished / 60000) % 60;
                        long sec = (millisUntilFinished / 1000) % 60;
                        String hourS = "";
                        if (hour != 0) {
                            hourS = f.format(hour) + ":";
                        }
                        String minS = "";
                        if (min != 0 || hour == 0) {
                            minS = f.format(min) + ":";
                        }
                        eye4.setText(Html.fromHtml("<b>ANOIKTO<br><small>♨ "+eyeDAO.find(4).getVolume()+" • "+ hourS + minS + f.format(sec) +"</small></b>"));
                    }

                    public void onFinish() {
                        eye4.setText("ΚΛΕΙΣΤΟ");
                        eye4.setTextColor(getResources().getColor(R.color.buttons_color));
                    }
                }.start();
            }
            else {
                eye4Flag = true;
                eye4.setText(Html.fromHtml("<b>ANOIKTO<br><small>♨ "+eyeDAO.find(4).getVolume()+"</small></b>"));
            }
        }
        else {
            eye4Flag = true;
            eye4.setText("ΚΛΕΙΣΤΟ");
            eye4.setTextColor(getResources().getColor(R.color.buttons_color));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }
}