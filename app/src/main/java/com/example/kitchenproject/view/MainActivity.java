package com.example.kitchenproject.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.kitchenproject.R;
import com.example.kitchenproject.dao.EyeDAO;
import com.example.kitchenproject.dao.MemoryInitializer;
import com.example.kitchenproject.dao.OvenDAO;
import com.example.kitchenproject.domain.Eye;
import com.example.kitchenproject.domain.Setting;
import com.example.kitchenproject.domain.TypeOfCooking;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private static boolean initialized = false;

    CountDownTimer countDownTimer;

    private Button eyesButton;
    private Button ovenButton;

    private Boolean ovenFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!initialized) {
            new MemoryInitializer().prepareData();
            initialized = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        setContentView(R.layout.activity_main);

        eyesButton = findViewById(R.id.Eyes);
        ovenButton = findViewById(R.id.Oven);

        ovenFlag = false;

        eyesButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainActivity.this, EyesActivity.class);
                startActivity(intent);
            }
        });

        ovenButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainActivity.this, OvenActivity.class);
                startActivity(intent);
            }
        });
        EyeDAO eyeDAO = new EyeDAO();

        int openedEyes = 0;
        for(Eye eye : eyeDAO.findAll()) {
            if (eye.getSetting() == Setting.ON) {
                openedEyes++;
            }
        }
        if (openedEyes == 0) {
            eyesButton.setText("Μάτια");
        }
        else if (openedEyes == 1) {
            eyesButton.setText(Html.fromHtml("Μάτια<br><span style='color: #169A00;'><small><small><b>" + openedEyes+"</b>&nbsp;&nbsp;ANOIKTΟ</small></small></span>"));
        }
        else {
            eyesButton.setText(Html.fromHtml("Μάτια<br><span style='color: #169A00;'><small><small><b>" + openedEyes+"</b>&nbsp;&nbsp;ANOIKTΑ</small></small></span>"));
        }

        OvenDAO ovenDAO = new OvenDAO();
        if (ovenDAO.get().getCooking() != TypeOfCooking.OFF) {
            ovenButton.setText(Html.fromHtml("Φούρνος<span style='color: #169A00;'><small><small><br>ANOIKTΟΣ<br><small><b>"+ovenDAO.get().getTemperature()+" °C</b></small></small></small></span>"));

            if (ovenDAO.get().getAutoOff() != null) {
                countDownTimer = new CountDownTimer(getDateDiff(Calendar.getInstance().getTime(), ovenDAO.get().getAutoOff(), TimeUnit.MILLISECONDS), 1000) {
                    public void onTick(long millisUntilFinished) {

                        if (ovenFlag) {
                            cancel();
                            return;
                        }

                        NumberFormat f = new DecimalFormat("00");
                        long hour = (millisUntilFinished / 3600000) % 24;
                        long min = (millisUntilFinished / 60000) % 60;
                        long sec = (millisUntilFinished / 1000) % 60;
                        String hourS = "";
                        if (hour != 0) {
                            hourS = f.format(hour) + ":";
                        }
                        String minS = "";
                        if (min != 0 || hour == 0) {
                            minS = f.format(min) + ":";
                        }
                        ovenButton.setText(Html.fromHtml("Φούρνος<span style='color: #169A00;'><small><small><br>ANOIKTΟΣ<br><small><b>"+ovenDAO.get().getTemperature()+" °C • "+ hourS + minS + f.format(sec) +"</b></small></small></small></span>"));
                    }

                    public void onFinish() {
                        ovenButton.setText("Φούρνος");
                        ovenButton.setTextColor(getResources().getColor(R.color.buttons_color));
                    }
                }.start();
            }
            else {
                ovenFlag = true;
                ovenButton.setText(Html.fromHtml("Φούρνος<span style='color: #169A00;'><small><small><br>ANOIKTΟΣ<br><small><b>"+ovenDAO.get().getTemperature()+" °C</b></small></small></small></span>"));
            }
        }
        else {
            ovenFlag = true;
            ovenButton.setText("Φούρνος");
            ovenButton.setTextColor(getResources().getColor(R.color.buttons_color));
        }
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }
}