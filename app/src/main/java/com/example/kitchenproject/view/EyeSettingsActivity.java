package com.example.kitchenproject.view;

import static java.lang.Thread.sleep;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.res.ResourcesCompat;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.kitchenproject.R;
import com.example.kitchenproject.dao.EyeDAO;
import com.example.kitchenproject.domain.Eye;
import com.example.kitchenproject.domain.Setting;
import com.google.android.material.button.MaterialButton;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class EyeSettingsActivity extends AppCompatActivity {
    EyeDAO eyeDAO = new EyeDAO();

    Date autoOff = null;

    final Context context = this;

    CountDownTimer countDownTimer;

    SwitchCompat eyeSwich;
    SwitchCompat autoOffSwich;
    int eyeId;
    TextView volume;
    SeekBar volumeSeekBar;
    MaterialButton help1;
    MaterialButton help2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eye_settings);

        Intent intent = getIntent();
        eyeId = intent.getIntExtra("eyeId", 1);
        Eye eye = eyeDAO.find(eyeId);

        if (eyeId == 1) {
            setTitle(R.string.eyeSettings1);
        }
        else if (eyeId == 2) {
            setTitle(R.string.eyeSettings2);
        }
        else if (eyeId == 3) {
            setTitle(R.string.eyeSettings3);
        }
        else {
            setTitle(R.string.eyeSettings4);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        eyeSwich = findViewById(R.id.switch1);
        autoOffSwich = findViewById(R.id.switch5);
        volume = findViewById(R.id.textView40);
        volumeSeekBar = findViewById(R.id.seekBar20);
        help1 = findViewById(R.id.button30);
        help2 = findViewById(R.id.button50);

        if (eye.getSetting() == Setting.ON) {
            eyeSwich.setChecked(true);
            eyeSwich.setText(Html.fromHtml("<span style='color:#FF1901;'>Ανοικτό</span>"));

            volume.setText(String.valueOf(eye.getVolume()));
            volumeSeekBar.setProgress(eye.getVolume()-1);

            if (eye.getAutoOff() != null) {
                autoOffSwich.setChecked(true);
                autoOffSwich.setText(Html.fromHtml("<span style='color:#FF1901;'>Ενεργό</span><br><small>Το μάτι θα κλείσει αυτόματα στις " + (eye.getAutoOff().getHours()<10?"0":"") + eye.getAutoOff().getHours() + ":" + (eye.getAutoOff().getMinutes()<10?"0":"") + eye.getAutoOff().getMinutes() + "</small>"));

                countDownTimer = new CountDownTimer(getDateDiff(Calendar.getInstance().getTime(), eye.getAutoOff(), TimeUnit.MILLISECONDS), 1000) {
                    public void onTick(long millisUntilFinished) {

                        NumberFormat f = new DecimalFormat("0");
                        long hour = (millisUntilFinished / 3600000) % 24;
                        long min = (millisUntilFinished / 60000) % 60;
                        long sec = (millisUntilFinished / 1000) % 60;
                        String hourS = "";
                        if (hour != 0) {
                            String ss = " ώρες ";
                            if (min == 1) {
                                ss = " ώρα ";
                            }
                            hourS = f.format(hour) + " ώρες ";
                        }
                        String minS = "";
                        if (min != 0) {
                            String ss = " λεπτά ";
                            if (min == 1) {
                                ss = " λεπτό ";
                            }
                            minS = f.format(min) + ss;
                        }
                        String secS = "";
                        if (sec != 0) {
                            secS = f.format(sec) + " δευτ.";
                        }
                        eyeSwich.setText(Html.fromHtml("<span style='color:#FF1901;'>Ανοικτό</span><br><small>Κλείσιμο σε " + hourS + minS + secS + "</small>"));
                    }

                    public void onFinish() {
                        eyeSwich.setChecked(false);
                        eyeSwich.setText("Κλειστό");
                        eyeSwich.setTextColor(getResources().getColor(R.color.buttons_color));

                        autoOffSwich.setChecked(false);
                        autoOffSwich.setText("Ανενεργό");
                        autoOffSwich.setTextColor(getResources().getColor(R.color.buttons_color));
                    }
                }.start();
            }
        }

        help1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Typeface face1 = ResourcesCompat.getFont(context, R.font.commissioner_semibold);
                Typeface face2 = ResourcesCompat.getFont(context, R.font.commissioner_medium);
                CustomTFSpan tfSpan = new CustomTFSpan(face1);
                SpannableString spannableString = new SpannableString("Ένταση ματιού");
                spannableString.setSpan(tfSpan, 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                AlertDialog.Builder builder1 = new AlertDialog.Builder(context, R.style.MyAlertDialog);
                LayoutInflater factory = LayoutInflater.from(context);
                final View view = factory.inflate(R.layout.alert_dialog_image2, null);
                builder1.setTitle(spannableString);
                builder1.setMessage(Html.fromHtml("<br>Η ένταση του ματιού ρυθμίζεται χρησιμοποιόντας την αντίστοιχη μπάρα.<br><br>Με το άγγιγμα του αντίχειρα, μπορείτε να σύρετε αριστερά ή δεξιά έτσι ώστε να ορίσετε την επιθυμητή ένταση.<br>"));
                builder1.setView(view);
                builder1.setIcon(R.drawable.fire_icon);
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        Html.fromHtml("<b>ΤΟ ΚΑΤΑΛΑΒΑ</b>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
                TextView textView = (TextView) alert11.findViewById(android.R.id.message);
                textView.setTypeface(face2);
            }
        });
        help1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(getApplicationContext(),"Βοήθεια σχετικά με την ένταση",Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        help2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Typeface face1 = ResourcesCompat.getFont(context, R.font.commissioner_semibold);
                Typeface face2 = ResourcesCompat.getFont(context, R.font.commissioner_medium);
                CustomTFSpan tfSpan = new CustomTFSpan(face1);
                SpannableString spannableString = new SpannableString("Αυτόματο κλείσιμο ματιού");
                spannableString.setSpan(tfSpan, 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                AlertDialog.Builder builder1 = new AlertDialog.Builder(context, R.style.MyAlertDialog);
                LayoutInflater factory = LayoutInflater.from(context);
                final View view = factory.inflate(R.layout.alert_dialog_image3, null);
                builder1.setTitle(spannableString);
                builder1.setMessage(Html.fromHtml("<br>Το μάτι μπορεί να κλείσει και αυτόματα.<br><br>Αφού έχετε ενεργοποιήσει τον αντίστοιχο μοχλό, μπορείτε να επιλέξετε την ώρα κλεισίματος. Αρχικά επιλέγετε την ώρα και ύστερα τα λεπτά.<br>"));
                builder1.setView(view);
                builder1.setIcon(R.drawable.time_icon);
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        Html.fromHtml("<b>ΤΟ ΚΑΤΑΛΑΒΑ</b>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
                TextView textView = (TextView) alert11.findViewById(android.R.id.message);
                textView.setTypeface(face2);
            }
        });
        help2.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(getApplicationContext(),"Βοήθεια σχετικά με το αυτόματο κλείσιμο",Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        eyeSwich.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    eyeSwich.setText(Html.fromHtml("<span style='color:#FF1901;'>Ανοικτό</span>"));

                    eye.setAutoOff(autoOff);
                    eye.setSetting(Setting.ON);

                    if (eye.getAutoOff() != null) {
                        countDownTimer = new CountDownTimer(getDateDiff(Calendar.getInstance().getTime(), eye.getAutoOff(), TimeUnit.MILLISECONDS), 1000) {
                            public void onTick(long millisUntilFinished) {

                                NumberFormat f = new DecimalFormat("0");
                                long hour = (millisUntilFinished / 3600000) % 24;
                                long min = (millisUntilFinished / 60000) % 60;
                                long sec = (millisUntilFinished / 1000) % 60;
                                String hourS = "";
                                if (hour != 0) {
                                    String ss = " ώρες ";
                                    if (min == 1) {
                                        ss = " ώρα ";
                                    }
                                    hourS = f.format(hour) + " ώρες ";
                                }
                                String minS = "";
                                if (min != 0) {
                                    String ss = " λεπτά ";
                                    if (min == 1) {
                                        ss = " λεπτό ";
                                    }
                                    minS = f.format(min) + ss;
                                }
                                String secS = f.format(sec) + " δευτ.";
                                eyeSwich.setText(Html.fromHtml("<span style='color:#FF1901;'>Ανοικτό</span><br><small>Κλείσιμο σε " + hourS + minS + secS + "</small>"));
                            }

                            public void onFinish() {
                                eyeSwich.setChecked(false);
                                eyeSwich.setText("Κλειστό");
                                eyeSwich.setTextColor(getResources().getColor(R.color.buttons_color));

                                autoOffSwich.setChecked(false);
                                autoOffSwich.setText("Ανενεργό");
                                autoOffSwich.setTextColor(getResources().getColor(R.color.buttons_color));
                            }
                        }.start();
                    }

                }
                else {
                    eyeSwich.setText("Κλειστό");

                    if (eye.getAutoOff() != null) {
                        countDownTimer.cancel();
                    }

                    eye.setAutoOff(null);
                    eye.setSetting(Setting.OFF);

                    autoOffSwich.setChecked(false);
                    autoOffSwich.setText("Ανενεργό");
                    autoOffSwich.setTextColor(getResources().getColor(R.color.buttons_color));
                }
            }
        });

        autoOffSwich.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Typeface face1 = ResourcesCompat.getFont(context, R.font.commissioner_semibold);
                    Typeface face2 = ResourcesCompat.getFont(context, R.font.commissioner_medium);
                    CustomTFSpan tfSpan = new CustomTFSpan(face1);
                    SpannableString spannableString = new SpannableString("Επιλέξτε ώρα κλεισίματος");
                    spannableString.setSpan(tfSpan, 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    Calendar mcurrentTime = Calendar.getInstance();
                    int year = mcurrentTime.get(Calendar.YEAR);
                    int month = mcurrentTime.get(Calendar.MONTH);
                    int day = mcurrentTime.get(Calendar.DAY_OF_MONTH);
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    Date currentDate = mcurrentTime.getTime();

                    TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            autoOffSwich.setChecked(true);
                            try {
                                autoOffSwich.setText(Html.fromHtml("<span style='color:#FF1901;'>Ενεργό</span><br><small>Το μάτι θα κλείσει αυτόματα στις " + (selectedHour<10?"0":"") + selectedHour + ":" + (selectedMinute<10?"0":"") + selectedMinute + "</small>"));
                                DateFormat dt = new SimpleDateFormat("d/M/yyyy HH:mm");

                                DateFormat dt2 = new SimpleDateFormat("HH:mm");
                                DateFormat dt3 = new SimpleDateFormat("HH:mm:ss");
                                Date pickedDate = dt2.parse(selectedHour + ":" + selectedMinute);
                                Date currDate = dt2.parse(hour + ":" + minute);
                                if (pickedDate.after(currDate) && pickedDate.before(dt3.parse("23:59:59"))) {
                                    pickedDate = dt.parse(day + "/" + (month+1) + "/" + year + " " + selectedHour + ":" + selectedMinute);
                                }
                                else {
                                    pickedDate = dt.parse((day + 1) + "/" + (month + 1) + "/" + year + " " + selectedHour + ":" + selectedMinute);
                                }
                                autoOff = pickedDate;
                                System.out.println(currentDate);
                                System.out.println(pickedDate);

                                if (eye.getSetting() == Setting.ON) {
                                    eye.setAutoOff(autoOff);

                                    if (eye.getAutoOff() != null) {
                                        countDownTimer = new CountDownTimer(getDateDiff(Calendar.getInstance().getTime(), eye.getAutoOff(), TimeUnit.MILLISECONDS), 1000) {
                                            public void onTick(long millisUntilFinished) {

                                                NumberFormat f = new DecimalFormat("0");
                                                long hour = (millisUntilFinished / 3600000) % 24;
                                                long min = (millisUntilFinished / 60000) % 60;
                                                long sec = (millisUntilFinished / 1000) % 60;
                                                String hourS = "";
                                                if (hour != 0) {
                                                    String ss = " ώρες ";
                                                    if (min == 1) {
                                                        ss = " ώρα ";
                                                    }
                                                    hourS = f.format(hour) + " ώρες ";
                                                }
                                                String minS = "";
                                                if (min != 0) {
                                                    String ss = " λεπτά ";
                                                    if (min == 1) {
                                                        ss = " λεπτό ";
                                                    }
                                                    minS = f.format(min) + ss;
                                                }
                                                String secS = f.format(sec) + " δευτ.";
                                                eyeSwich.setText(Html.fromHtml("<span style='color:#FF1901;'>Ανοικτό</span><br><small>Κλείσιμο σε " + hourS + minS + secS + "</small>"));
                                            }

                                            public void onFinish() {
                                                eyeSwich.setChecked(false);
                                                eyeSwich.setText("Κλειστό");
                                                eyeSwich.setTextColor(getResources().getColor(R.color.buttons_color));

                                                autoOffSwich.setChecked(false);
                                                autoOffSwich.setText("Ανενεργό");
                                                autoOffSwich.setTextColor(getResources().getColor(R.color.buttons_color));
                                            }
                                        }.start();
                                    }
                                }

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    TimePickerDialog myTPDialog = new TimePickerDialog(context, R.style.MyTimerDialog, mTimeSetListener,hour,minute,true);
                    myTPDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            autoOffSwich.setChecked(false);
                        }
                    });
                    myTPDialog.setTitle(spannableString);
                    myTPDialog.show();

                    TextView textView = (TextView) myTPDialog.findViewById(android.R.id.message);
                    textView.setTypeface(face2);
                }
                else {
                    if (eye.getSetting() == Setting.ON) {
                        eye.setAutoOff(null);
                        if (countDownTimer != null) {
                            countDownTimer.cancel();
                        }
                        eyeSwich.setText("Ανοικτό");
                        autoOffSwich.setTextColor(getResources().getColor(R.color.main_color));
                    }

                    autoOffSwich.setText("Ανενεργό");
                    autoOffSwich.setTextColor(getResources().getColor(R.color.buttons_color));
                    autoOffSwich.setChecked(false);
                }
            }
        });

        volume.setText(String.valueOf(1+volumeSeekBar.getProgress()));
        eye.setVolume(1+volumeSeekBar.getProgress());
        volumeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged (SeekBar seekBar, int progress, boolean fromUser){
                volume.setText(String.valueOf(1+volumeSeekBar.getProgress()));
                eye.setVolume(1+volumeSeekBar.getProgress());

                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(20);
            }
            @Override public void onStartTrackingTouch(SeekBar seekBar){
            }
            @Override public void onStopTrackingTouch(SeekBar seekBar){
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }
}