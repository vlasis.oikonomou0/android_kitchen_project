package com.example.kitchenproject.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.res.ResourcesCompat;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;


import com.example.kitchenproject.R;
import com.example.kitchenproject.dao.EyeDAO;
import com.example.kitchenproject.dao.OvenDAO;
import com.example.kitchenproject.domain.Eye;
import com.example.kitchenproject.domain.Oven;
import com.example.kitchenproject.domain.Setting;
import com.example.kitchenproject.domain.TypeOfCooking;
import com.google.android.material.button.MaterialButton;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class OvenActivity extends AppCompatActivity {
    OvenDAO ovenDAO = new OvenDAO();

    Date autoOff = null;

    final Context context = this;

    CountDownTimer countDownTimer;

    SwitchCompat ovenSwich;
    SwitchCompat autoOffSwich;
    TextView temperature;
    SeekBar temperatureSeekBar;
    RadioGroup radioGroup;
    MaterialButton help1;
    MaterialButton help2;
    MaterialButton help3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oven);

        setTitle(R.string.oven);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Oven oven = ovenDAO.get();

        ovenSwich = findViewById(R.id.switch2);
        autoOffSwich = findViewById(R.id.switch7);
        temperature = findViewById(R.id.textView4);
        temperatureSeekBar = findViewById(R.id.seekBar2);
        radioGroup = findViewById(R.id.radioGroup2);
        help1 = findViewById(R.id.button3);
        help2 = findViewById(R.id.button2);
        help3 = findViewById(R.id.button70);
        RadioButton checkedRadioButton = (RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());

        int count = radioGroup.getChildCount();
        ArrayList<RadioButton> listOfRadioButtons = new ArrayList<RadioButton>();
        for (int i=0;i<count;i++) {
            View o = radioGroup.getChildAt(i);
            if (o instanceof RadioButton) {
                listOfRadioButtons.add((RadioButton)o);
            }
        }

        if (oven.getCooking() != TypeOfCooking.OFF) {
            ovenSwich.setChecked(true);
            ovenSwich.setText(Html.fromHtml("<span style='color:#FF1901;'>Ανοικτός</span>"));

            temperature.setText(String.valueOf(oven.getTemperature() + " °C"));
            temperatureSeekBar.setProgress((oven.getTemperature()-50)/10);

            for (RadioButton rb : listOfRadioButtons) {
                if (String.valueOf(oven.getCooking()).equals(getResources().getResourceEntryName(rb.getId()))) {
                    rb.setChecked(true);

                    break;
                }
            }

            if (oven.getAutoOff() != null) {
                autoOffSwich.setChecked(true);
                autoOffSwich.setText(Html.fromHtml("<span style='color:#FF1901;'>Ενεργό</span><br><small>Ο φούρνος θα κλείσει αυτόματα στις " + (oven.getAutoOff().getHours()<10?"0":"") + oven.getAutoOff().getHours() + ":" + (oven.getAutoOff().getMinutes()<10?"0":"") + oven.getAutoOff().getMinutes() + "</small>"));

                countDownTimer = new CountDownTimer(getDateDiff(Calendar.getInstance().getTime(), oven.getAutoOff(), TimeUnit.MILLISECONDS), 1000) {
                    public void onTick(long millisUntilFinished) {

                        NumberFormat f = new DecimalFormat("0");
                        long hour = (millisUntilFinished / 3600000) % 24;
                        long min = (millisUntilFinished / 60000) % 60;
                        long sec = (millisUntilFinished / 1000) % 60;
                        String hourS = "";
                        if (hour != 0) {
                            String ss = " ώρες ";
                            if (min == 1) {
                                ss = " ώρα ";
                            }
                            hourS = f.format(hour) + " ώρες ";
                        }
                        String minS = "";
                        if (min != 0) {
                            String ss = " λεπτά ";
                            if (min == 1) {
                                ss = " λεπτό ";
                            }
                            minS = f.format(min) + ss;
                        }
                        String secS = "";
                        if (sec != 0) {
                            secS = f.format(sec) + " δευτ.";
                        }
                        ovenSwich.setText(Html.fromHtml("<span style='color:#FF1901;'>Ανοικτός</span><br><small>Κλείσιμο σε " + hourS + minS + secS + "</small>"));
                    }

                    public void onFinish() {
                        ovenSwich.setChecked(false);
                        ovenSwich.setText("Κλειστός");
                        ovenSwich.setTextColor(getResources().getColor(R.color.buttons_color));

                        autoOffSwich.setChecked(false);
                        autoOffSwich.setText("Ανενεργό");
                        autoOffSwich.setTextColor(getResources().getColor(R.color.buttons_color));
                    }
                }.start();
            }

        }

        help1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Typeface face1 = ResourcesCompat.getFont(context, R.font.commissioner_semibold);
                Typeface face2 = ResourcesCompat.getFont(context, R.font.commissioner_medium);
                CustomTFSpan tfSpan = new CustomTFSpan(face1);
                SpannableString spannableString = new SpannableString("Θερμοκρασία φούρνου");
                spannableString.setSpan(tfSpan, 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                AlertDialog.Builder builder1 = new AlertDialog.Builder(context, R.style.MyAlertDialog);
                LayoutInflater factory = LayoutInflater.from(context);
                final View view = factory.inflate(R.layout.alert_dialog_image, null);
                builder1.setTitle(spannableString);
                builder1.setMessage(Html.fromHtml("<br>Η θερμοκρασία του φούρνου σας ρυθμίζεται χρησιμοποιόντας την αντίστοιχη μπάρα.<br><br>Με το άγγιγμα του αντίχειρα, μπορείτε να σύρετε αριστερά ή δεξιά έτσι ώστε να ορίσετε την επιθυμητή θερμοκρασία.<br>"));
                builder1.setView(view);
                builder1.setIcon(R.drawable.temperature_icon);
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        Html.fromHtml("<b>ΤΟ ΚΑΤΑΛΑΒΑ</b>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
                TextView textView = (TextView) alert11.findViewById(android.R.id.message);
                textView.setTypeface(face2);
            }
        });
        help1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(getApplicationContext(),"Βοήθεια σχετικά με την θερμοκρασία",Toast.LENGTH_SHORT).show();
                return true;
            }
        });


        help2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Typeface face1 = ResourcesCompat.getFont(context, R.font.commissioner_semibold);
                Typeface face2 = ResourcesCompat.getFont(context, R.font.commissioner_medium);
                CustomTFSpan tfSpan = new CustomTFSpan(face1);
                SpannableString spannableString = new SpannableString("Λειτουργίες φούρνου");
                spannableString.setSpan(tfSpan, 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                AlertDialog.Builder builder1 = new AlertDialog.Builder(context, R.style.MyAlertDialog);
                LayoutInflater factory = LayoutInflater.from(context);
                final View view = factory.inflate(R.layout.oven_functions_icon, null);
                builder1.setTitle(spannableString);
                builder1.setMessage(Html.fromHtml("<br><b>Πάνω-κάτω</b><br>Ψήνουμε φαγητά με λαχανικά όπως γεμιστά, μελιτζάνες με τομάτα και φέτα, ιμάμ μπαϊλντί, μπριάμ, πίτες αλλά και γλυκά.<br><br>" +
                        "<b>Πάνω</b><br>Χρησιμοποιούμε την πάνω αντίσταση όταν θέλουμε στο τελείωμα του ψησίματος να πάρει λίγο χρώμα το φαγητό μας.<br><br>" +
                        "<b>Κάτω</b><br>Χρησιμοποιούμε την κάτω αντίσταση όταν θέλουμε να ψηθεί το κάτω μέρος του φαγητού μας λίγο περισσότερο.<br><br>" +
                        "<b>Γκριλ</b><br>Χρησιμοποιούμε το γκριλ όταν θέλουμε να ψήσουμε κρέας, ψάρι ή ψητά λαχανικά.<br><br>" +
                        "<b>Αέρας</b><br>Χρησιμοποιούμε τον αέρα όταν θέλουμε να ψήσουμε το φαγητό ομοιόμορφα σε ένα ζεστό στρώμα αέρα.<br>"));
                builder1.setView(view);
                builder1.setIcon(R.drawable.oven_icon);
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        Html.fromHtml("<b>ΤΟ ΚΑΤΑΛΑΒΑ</b>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
                TextView textView = (TextView) alert11.findViewById(android.R.id.message);
                textView.setTypeface(face2);
            }
        });
        help2.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(getApplicationContext(),"Βοήθεια σχετικά με τις λειτουργίες",Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        help3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Typeface face1 = ResourcesCompat.getFont(context, R.font.commissioner_semibold);
                Typeface face2 = ResourcesCompat.getFont(context, R.font.commissioner_medium);
                CustomTFSpan tfSpan = new CustomTFSpan(face1);
                SpannableString spannableString = new SpannableString("Αυτόματο κλείσιμο φούρνου");
                spannableString.setSpan(tfSpan, 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                AlertDialog.Builder builder1 = new AlertDialog.Builder(context, R.style.MyAlertDialog);
                LayoutInflater factory = LayoutInflater.from(context);
                final View view = factory.inflate(R.layout.alert_dialog_image3, null);
                builder1.setTitle(spannableString);
                builder1.setMessage(Html.fromHtml("<br>Το φούρνος μπορεί να κλείσει και αυτόματα.<br><br>Αφού έχετε ενεργοποιήσει τον αντίστοιχο μοχλό, μπορείτε να επιλέξετε την ώρα κλεισίματος. Αρχικά επιλέγετε την ώρα και ύστερα τα λεπτά.<br>"));
                builder1.setView(view);
                builder1.setIcon(R.drawable.time_icon);
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        Html.fromHtml("<b>ΤΟ ΚΑΤΑΛΑΒΑ</b>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
                TextView textView = (TextView) alert11.findViewById(android.R.id.message);
                textView.setTypeface(face2);
            }
        });
        help3.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(getApplicationContext(),"Βοήθεια σχετικά με το αυτόματο κλείσιμο",Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                boolean isChecked = checkedRadioButton.isChecked();

                if (oven.getCooking() != TypeOfCooking.OFF) {
                    if (isChecked) {
                        for (RadioButton rb : listOfRadioButtons) {
                            if (rb.isChecked()) {
                                oven.setCooking(TypeOfCooking.valueOf(getResources().getResourceEntryName(rb.getId())));
                                break;
                            }
                        }
                    }
                }
            }
        });

        ovenSwich.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (radioGroup.getCheckedRadioButtonId() == -1) {

                        Typeface face1 = ResourcesCompat.getFont(context, R.font.commissioner_semibold);
                        Typeface face2 = ResourcesCompat.getFont(context, R.font.commissioner_medium);
                        CustomTFSpan tfSpan = new CustomTFSpan(face1);
                        SpannableString spannableString = new SpannableString("Μια στιγμή!");
                        spannableString.setSpan(tfSpan, 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(context, R.style.MyAlertDialog);
                        builder1.setTitle(spannableString);
                        builder1.setMessage("Πρέπει πρώτα να επιλέξτε μία από τις διαθέσιμες λειτουργίες για να ενεργοποιηθεί ο φούρνος.");
                        builder1.setIcon(R.drawable.warning_icon);
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                Html.fromHtml("<b>ΟΚ</b>"),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        /*
                        builder1.setNegativeButton(
                                "ΑΚΥΡΟ",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                           */

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        TextView textView = (TextView) alert11.findViewById(android.R.id.message);
                        textView.setTypeface(face2);

                        ovenSwich.setChecked(false);
                    }
                    else {
                        oven.setAutoOff(autoOff);

                        ovenSwich.setText(Html.fromHtml("<span style='color:#FF1901;'>Ανοικτός</span>"));

                        for (RadioButton rb : listOfRadioButtons) {
                            if (rb.isChecked()) {
                                oven.setCooking(TypeOfCooking.valueOf(getResources().getResourceEntryName(rb.getId())));
                                break;
                            }
                        }

                        if (oven.getAutoOff() != null) {
                            countDownTimer = new CountDownTimer(getDateDiff(Calendar.getInstance().getTime(), oven.getAutoOff(), TimeUnit.MILLISECONDS), 1000) {
                                public void onTick(long millisUntilFinished) {

                                    NumberFormat f = new DecimalFormat("0");
                                    long hour = (millisUntilFinished / 3600000) % 24;
                                    long min = (millisUntilFinished / 60000) % 60;
                                    long sec = (millisUntilFinished / 1000) % 60;
                                    String hourS = "";
                                    if (hour != 0) {
                                        String ss = " ώρες ";
                                        if (min == 1) {
                                            ss = " ώρα ";
                                        }
                                        hourS = f.format(hour) + " ώρες ";
                                    }
                                    String minS = "";
                                    if (min != 0) {
                                        String ss = " λεπτά ";
                                        if (min == 1) {
                                            ss = " λεπτό ";
                                        }
                                        minS = f.format(min) + ss;
                                    }
                                    String secS = f.format(sec) + " δευτ.";
                                    ovenSwich.setText(Html.fromHtml("<span style='color:#FF1901;'>Ανοικτός</span><br><small>Κλείσιμο σε " + hourS + minS + secS + "</small>"));
                                }

                                public void onFinish() {
                                    ovenSwich.setChecked(false);
                                    ovenSwich.setText("Κλειστός");
                                    ovenSwich.setTextColor(getResources().getColor(R.color.buttons_color));

                                    autoOffSwich.setChecked(false);
                                    autoOffSwich.setText("Ανενεργό");
                                    autoOffSwich.setTextColor(getResources().getColor(R.color.buttons_color));
                                }
                            }.start();
                        }
                    }
                }
                else {
                    ovenSwich.setText("Κλειστός");
                    ovenSwich.setTextColor(getResources().getColor(R.color.buttons_color));

                    if (oven.getAutoOff() != null) {
                        countDownTimer.cancel();
                    }

                    oven.setAutoOff(null);
                    oven.setCooking(TypeOfCooking.OFF);

                    autoOffSwich.setChecked(false);
                    autoOffSwich.setText("Ανενεργό");
                    autoOffSwich.setTextColor(getResources().getColor(R.color.buttons_color));
                }
            }
        });

        autoOffSwich.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Typeface face1 = ResourcesCompat.getFont(context, R.font.commissioner_semibold);
                    Typeface face2 = ResourcesCompat.getFont(context, R.font.commissioner_medium);
                    CustomTFSpan tfSpan = new CustomTFSpan(face1);
                    SpannableString spannableString = new SpannableString("Επιλέξτε ώρα κλεισίματος");
                    spannableString.setSpan(tfSpan, 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    Calendar mcurrentTime = Calendar.getInstance();
                    int year = mcurrentTime.get(Calendar.YEAR);
                    int month = mcurrentTime.get(Calendar.MONTH);
                    int day = mcurrentTime.get(Calendar.DAY_OF_MONTH);
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    Date currentDate = mcurrentTime.getTime();

                    TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            autoOffSwich.setChecked(true);
                            try {
                                autoOffSwich.setText(Html.fromHtml("<span style='color:#FF1901;'>Ενεργό</span><br><small>Ο φούρνος θα κλείσει αυτόματα στις " + (selectedHour<10?"0":"") + selectedHour + ":" + (selectedMinute<10?"0":"") + selectedMinute + "</small>"));
                                DateFormat dt = new SimpleDateFormat("d/M/yyyy HH:mm");

                                DateFormat dt2 = new SimpleDateFormat("HH:mm");
                                DateFormat dt3 = new SimpleDateFormat("HH:mm:ss");
                                Date pickedDate = dt2.parse(selectedHour + ":" + selectedMinute);
                                Date currDate = dt2.parse(hour + ":" + minute);
                                Date currDatePlus1 = dt2.parse(hour + ":" + minute+1);
                                if (pickedDate.after(currDate) && pickedDate.before(dt3.parse("23:59:59"))) {
                                    pickedDate = dt.parse(day + "/" + (month+1) + "/" + year + " " + selectedHour + ":" + selectedMinute);
                                }
                                else {
                                    pickedDate = dt.parse((day + 1) + "/" + (month + 1) + "/" + year + " " + selectedHour + ":" + selectedMinute);
                                }
                                autoOff = pickedDate;
                                System.out.println(currentDate);
                                System.out.println(pickedDate);

                                if (oven.getCooking() != TypeOfCooking.OFF) {
                                    oven.setAutoOff(autoOff);

                                    if (oven.getAutoOff() != null) {
                                        countDownTimer = new CountDownTimer(getDateDiff(Calendar.getInstance().getTime(), oven.getAutoOff(), TimeUnit.MILLISECONDS), 1000) {
                                            public void onTick(long millisUntilFinished) {

                                                NumberFormat f = new DecimalFormat("0");
                                                long hour = (millisUntilFinished / 3600000) % 24;
                                                long min = (millisUntilFinished / 60000) % 60;
                                                long sec = (millisUntilFinished / 1000) % 60;
                                                String hourS = "";
                                                if (hour != 0) {
                                                    String ss = " ώρες ";
                                                    if (min == 1) {
                                                        ss = " ώρα ";
                                                    }
                                                    hourS = f.format(hour) + " ώρες ";
                                                }
                                                String minS = "";
                                                if (min != 0) {
                                                    String ss = " λεπτά ";
                                                    if (min == 1) {
                                                        ss = " λεπτό ";
                                                    }
                                                    minS = f.format(min) + ss;
                                                }
                                                String secS = f.format(sec) + " δευτ.";
                                                ovenSwich.setText(Html.fromHtml("<span style='color:#FF1901;'>Ανοικτός</span><br><small>Κλείσιμο σε " + hourS + minS + secS + "</small>"));
                                            }

                                            public void onFinish() {
                                                ovenSwich.setChecked(false);
                                                ovenSwich.setText("Κλειστός");
                                                ovenSwich.setTextColor(getResources().getColor(R.color.buttons_color));

                                                autoOffSwich.setChecked(false);
                                                autoOffSwich.setText("Ανενεργό");
                                                autoOffSwich.setTextColor(getResources().getColor(R.color.buttons_color));
                                            }
                                        }.start();
                                    }
                                }

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    TimePickerDialog myTPDialog = new TimePickerDialog(context, R.style.MyTimerDialog, mTimeSetListener,hour,minute,true);
                    myTPDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            autoOffSwich.setChecked(false);
                        }
                    });
                    myTPDialog.setTitle(spannableString);
                    myTPDialog.show();

                    TextView textView = (TextView) myTPDialog.findViewById(android.R.id.message);
                    textView.setTypeface(face2);
                }
                else {
                    if (oven.getCooking() != TypeOfCooking.OFF) {
                        oven.setAutoOff(null);
                        if (countDownTimer != null) {
                            countDownTimer.cancel();
                        }
                        ovenSwich.setText(Html.fromHtml("<span style='color:#FF1901;'>Ανοικτός</span>"));
                    }

                    autoOffSwich.setText("Ανενεργό");
                    autoOffSwich.setTextColor(getResources().getColor(R.color.buttons_color));
                    autoOffSwich.setChecked(false);
                }
            }
        });

        temperature.setText(String.valueOf(50+(temperatureSeekBar.getProgress()*10)+" °C"));
        oven.setTemperature(50+(temperatureSeekBar.getProgress()*10));
        temperatureSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged (SeekBar seekBar, int progress, boolean fromUser){
                temperature.setText(String.valueOf(50+(temperatureSeekBar.getProgress()*10)+" °C"));
                oven.setTemperature(50+(temperatureSeekBar.getProgress()*10));

                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(20);
            }
            @Override public void onStartTrackingTouch(SeekBar seekBar){
            }
            @Override public void onStopTrackingTouch(SeekBar seekBar){
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }
}