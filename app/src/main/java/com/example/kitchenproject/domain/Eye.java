package com.example.kitchenproject.domain;

import java.util.Date;

public class Eye {
    private int volume;
    private Date autoOff;
    private int eyeNumber;
    private Setting setting;

    public Eye(int eyeId) {
        volume = 5;
        autoOff = null;
        eyeNumber = eyeId;
        setting = Setting.OFF;
    }

    public void setSetting(Setting setting) {
        this.setting = setting;
    }

    public void setEyeNumber(int stoveNumber) {
        this.eyeNumber = stoveNumber;
    }

    public void setAutoOff(Date autoOff) {
        this.autoOff = autoOff;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public Date getAutoOff() {
        return autoOff;
    }

    public int getVolume() {
        return volume;
    }

    public int getEyeNumber() {
        return eyeNumber;
    }

    public Setting getSetting() {
        return setting;
    }
}