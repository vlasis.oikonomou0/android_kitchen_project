package com.example.kitchenproject.domain;

public enum Setting {
    ON,
    OFF
}