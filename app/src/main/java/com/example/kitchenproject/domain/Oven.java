package com.example.kitchenproject.domain;

import java.util.Date;

public class Oven {
    private int temperature;
    private Date autoOff;
    private TypeOfCooking cooking;

    public Oven() {
        temperature = 150;
        autoOff = null;
        cooking = TypeOfCooking.OFF;
    }

    public void setCooking(TypeOfCooking cooking) {
        this.cooking = cooking;
    }

    public void setAutoOff(Date autoOff) {
        this.autoOff = autoOff;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public Date getAutoOff() {
        return autoOff;
    }

    public int getTemperature() {
        return temperature;
    }

    public TypeOfCooking getCooking() {
        return cooking;
    }
}