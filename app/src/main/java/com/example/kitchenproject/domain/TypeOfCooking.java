package com.example.kitchenproject.domain;

public enum TypeOfCooking {
    OFF,
    GRILL,
    FAN,
    TOP_BOTTOM,
    TOP,
    BOTTOM
}